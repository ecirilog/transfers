package com.yp.demo.config.exception.handler;

import com.yp.demo.common.exception.YpException;
import com.yp.demo.common.exception.YpExceptionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {YpException.class})
    public ResponseEntity<YpExceptionResponse> defaultErrorHandler(YpException ex) {
        log.error("Exception occurred", ex);

        return null;
    }
}
