package com.yp.demo.dao.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Transfer {

    @Id
    private int id;
    private String guid;
    private double amount;
}
